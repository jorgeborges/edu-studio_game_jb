require 'studio_game_jb/dice'

module StudioGameJB
  describe Dice do
    it "should have a number between 1 and 6 assigned after creation" do
      dice = Dice.new

      dice.number.should be_between 1, 6
    end
    it "a roll should return a number between 1 and 6" do
      dice = Dice.new

      dice.roll.should be_between 1, 6
    end
  end
end
