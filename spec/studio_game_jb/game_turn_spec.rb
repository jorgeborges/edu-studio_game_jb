require 'studio_game_jb/game_turn'

module StudioGameJB
  describe GameTurn do
    include GameTurn

    before do
      $stdout = StringIO.new
      @initial_health = 120
      @player = Player.new "larry", @initial_health
    end

    it "w00ts the player if a high number (5 or 6) is rolled" do
      Dice.any_instance.stub(:roll).and_return 5

      GameTurn.take_turn(@player)

      @player.health.should == @initial_health + 15

      Dice.any_instance.stub(:roll).and_return 6

      GameTurn.take_turn(@player)

      @player.health.should == @initial_health + 15 * 2
    end

    it "skips the player if a medium number (3 or 4) is rolled" do
      Dice.any_instance.stub(:roll).and_return 3

      GameTurn.take_turn(@player)

      @player.health.should == @initial_health

      Dice.any_instance.stub(:roll).and_return 4

      GameTurn.take_turn(@player)

      @player.health.should == @initial_health
    end

    it "blams the player if a low number (1 or 2) is rolled" do
      Dice.any_instance.stub(:roll).and_return 1

      GameTurn.take_turn(@player)

      @player.health.should == @initial_health - 10

      Dice.any_instance.stub(:roll).and_return 2

      GameTurn.take_turn(@player)

      @player.health.should == @initial_health - 10 * 2
    end

    it "assigns a treasure for points during a player's turn" do
      GameTurn.take_turn(@player)

      @player.points.should_not be_zero
    end
  end
end
