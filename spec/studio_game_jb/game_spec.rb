require 'studio_game_jb/game'

module StudioGameJB
  describe Game do
    before do
      $stdout = StringIO.new
      @game = Game.new("Knuckleheads")

      @initial_health = 100
      @player = Player.new("moe", @initial_health)

      @game.add_player(@player)
    end

    it "returns the number of players" do
      @game.number_of_players.should == 1
    end

    it "adds a player to the game" do
      @game.add_player Player.new "chester"
      @game.number_of_players.should == 2
    end

    it "calculates the stats for all the players" do
      player2 = Player.new "larry", 95
      player3 = Player.new "chester", 80

      @game.add_player player2
      @game.add_player player3

      Dice.any_instance.stub(:roll).and_return 5

      @game.play 1

      strong_players, winpy_players = @game.calculate_stats

      strong_players.should have(2).items
      winpy_players.should have(1).items
    end

    it "computes total points as the sum of all player points" do
      game = Game.new("Knuckleheads")

      player1 = Player.new("moe")
      player2 = Player.new("larry")

      game.add_player(player1)
      game.add_player(player2)

      player1.found_treasure(Treasure.new(:hammer, 50))
      player1.found_treasure(Treasure.new(:hammer, 50))
      player2.found_treasure(Treasure.new(:crowbar, 400))

      game.total_points.should == 500
    end

    it "plays for the number of specified rounds when no conditional block is passed" do
      Dice.any_instance.stub(:roll).and_return 3
      @game.play 10
      @player.health.should == @initial_health
    end

    it "plays until the conditional block is true and a winning criteria is met" do
      Dice.any_instance.stub(:roll).and_return 5
      TreasureTrove.stub(:random).and_return Treasure.new(:pie, 5)
      @game.play 10 do
        @game.total_points >= 10
      end
      @game.total_points.should == 10
      @player.health.should == @initial_health + 30
    end

    it "loads players correctly from a CSV file" do
      players_file = File.join(File.dirname(__FILE__), '../support/players.csv')
      @game.load_players players_file
      @game.number_of_players.should == 4
    end
  end
end
