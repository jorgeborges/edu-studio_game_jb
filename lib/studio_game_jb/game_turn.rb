require_relative 'dice'
require_relative 'loaded_dice'
require_relative 'player'
require_relative 'treasure_trove'

module StudioGameJB
  module GameTurn
    def self.take_turn(player)
      dice = Dice.new

      case dice.roll
      when 1..2
        player.blam
      when 3..4
        puts "#{player.name} was skipped."
      else
        player.w00t
      end

      player.found_treasure TreasureTrove.random
    end
  end
end
