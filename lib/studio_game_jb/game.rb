require 'csv'

require_relative 'game_turn'
require_relative 'player'
require_relative 'clumsy_player'
require_relative 'berserk_player'
require_relative 'treasure_trove'

module StudioGameJB
  class Game
    attr_reader :title

    def initialize(title)
      @title = title
      @players = []
    end

    def add_player(player)
      @players << player
    end

    def load_players(from_file)
      CSV.foreach(from_file) do |row|
        player = Player.new(row[0], Integer(row[1]))
        add_player player
      end
    end

    def save_high_scores(to_file="high_scores.txt")
      File.open(to_file, "w") do |file|
        file.puts "#{@title} High Scores:"
        @players.sort.each do |player|
          file.puts high_score_entry(player)
        end
      end
    end

    def high_score_entry(player)
      formatted_name = player.name.ljust(20, '.')
      "#{formatted_name} #{player.score}"
    end

    def number_of_players
      @players.size
    end

    def calculate_stats
      @players.partition {|player| player.strong?}
    end

    def total_points
      @players.reduce(0) {|sum, player| sum += player.points}
    end

    def play(rounds)
      puts "There are #{@players.size} players in #{@title}.\n"

      @players.each do |player|
        puts player
      end

      treasures = TreasureTrove::TREASURES

      puts "\nThere are #{treasures.size} treasures to be found:"
      treasures.each do |treasure|
        puts "A #{treasure.name} is worth #{treasure.points} points"
      end

      1.upto rounds do |round|
        break if block_given? and yield
        puts "\nRound #{round}:"
        @players.each do |player|
          GameTurn.take_turn player
          puts player
        end
      end
    end

    def print_stats
      strong_players, wimpy_players = calculate_stats
      puts "\n#{@title} Statistics:"

      puts "\n#{strong_players.size} strong players:"
      strong_players.each do |player|
        puts player.print_name_and_health
      end

      puts "\n#{wimpy_players.size} wimpy players:"
      wimpy_players.each do |player|
        puts player.print_name_and_health
      end

      @players.each do |player|
        puts "\n#{player.name}'s point totals:"
        player.each_found_treasure do |treasure|
          puts "#{treasure.points} total #{treasure.name} points"
        end
        puts "#{player.points} grand total points"
      end
      puts "\n#{total_points} total points from treasures found"

      puts "\n#{@title} High Scores:"
      @players.sort.each do |player|
        puts high_score_entry(player)
      end
    end
  end
end
