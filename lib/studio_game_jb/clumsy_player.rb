require_relative 'player'

module StudioGameJB
  class ClumsyPlayer < Player
    attr_reader :boost_factor

    def initialize(name, health = 100, boost_factor = 1)
      super name, health
      @boost_factor = boost_factor
    end

    def w00t
      @boost_factor.times { super }
    end

    def found_treasure(treasure)
      super Treasure.new treasure.name, treasure.points / 2
    end
  end
end
