# studio_game_jb

A simple CLI game made for the Pragmatic Studio Ruby course.

Install & Usage
--------

```shell
gem install studio_game_jb
```

Then, to run a game:

```shell
studio_game_jb
```

And input the number of rounds to play.

You may also load you own players by creating a CSV file (name, health) and passing it as a parameter:

```shell
studio_game_jb my_players.csv
```

More Information
----------------

* [Pragmatic Studio Ruby Course](http://pragmaticstudio.com/ruby)

License
-------

studio_game_jb is Copyright © 2013 Jorge A. Borges Colina. It is free software, and may be redistributed under the terms specified in the [LICENSE](https://github.com/jorgeborges/studio_game_jb/blob/master/LICENSE) file.
