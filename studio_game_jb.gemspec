Gem::Specification.new do |s|
  s.name         = "studio_game_jb"
  s.version      = "1.1.0"
  s.license      = "MIT"
  s.author       = "Jorge Alberto Borges Colina"
  s.email        = "jborges82@gmail.com"
  s.homepage     = "https://github.com/jorgeborges/studio_game_jb"
  s.summary      = "A simple CLI game made for the Pragmatic Studio Ruby course."
  s.description  = %q{A simple CLI game made for the Pragmatic Studio Ruby course.}

  s.files         = Dir["{bin,lib,spec}/**/*"] + %w(LICENSE README.md)
  s.test_files    = Dir["spec/**/*"]
  s.executables   = [ 'studio_game_jb' ]

  s.required_ruby_version = '>=1.9'
  s.add_development_dependency 'rspec'
end
